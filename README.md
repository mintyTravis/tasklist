# TaskList

A small quick project for practice with file editing.

The program will run through options. You can give it a new task, update an existing one, remove a task, or view a task list. It will write to and modify the task file.