#include <iostream>
#include <vector>

inline int argscheck(int argc, char* argv[])
{
    int theArg = 0;
    std::vector<std::string> programArgs = {"add","remove","update","list", "clear"};
    if (argc > 2){
        std::cout << "Error: will only allow one argument!" << std::endl;
        return 0;
    }
    else if (argc == 1) {
        std::cout << "You must choose a method to execute! (add, remove, update, or list)" << std::endl;
    }
    for (int i = 0; i < programArgs.size(); i++) {
        if (argv[1] == programArgs[i]) {
            std::cout << "Executing method: " << programArgs[i] << std::endl;
            return i + 1;
        }
    }
    std::cout <<"Not an acceptable command! (add, remove, update, or list)" << std::endl;
    return 0;
}