#include <iostream>
#include "tasks.h"
#include <vector>
#include "errorhandler.h"


int main(int argc, char* argv[])
{
    //todo: work on update / list
    //todo: remove has some bugs, user able to break choice of index
    
    int executeCommand = 0;
    executeCommand = argscheck(argc, argv);

    task myTask;

    switch (executeCommand){
        case 0:{
            return 0;
        }
        case 1:{
            if (openfile()){
                myTask.addNewTask();
            }
            else {
                std::cout << "Unable to run openfile() " << std::endl;
            }
            break;
        }
        case 2:{
            //todo - run remove
            if (openfile()){
                myTask.removeTask();
            }
            break;
        }
        case 3:{
            //todo - run update
            break;
        }
        case 4: {
            if (openfile()){
                myTask.listTasks();
            }
            break;
        }
        case 5: {
            if (openfile()){
                myTask.clearAll();
            }
            break;
        }
        default: {
            return 0;
        }
    }

    
    /*
    for(int i = 1; i < argc; i++){
        //std::cout << "->" << argv[i] << std::endl;
        for(int i2 = 0; i2 < programArgs.size();i2++){
           // std::cout << "--> Compare: " << argv[i] << " to: " << programArgs[i2] << std::endl;
            if (argv[i] == programArgs[i2]){
                acceptedArg = true;}
        }
        if (!acceptedArg){
            std::cout << "Arguments not accepted: " << argv[i] << std::endl;
            return 0;}
    }

    switch
    /*



    /*
    std::cout << "Running 'Task' version 1: " << std::endl;

    task firstask;
    std::string name;
    std::string description;

    std::cout << "Current task name and description" << std:: endl
    << "Name: " << firstask.taskName << std::endl
    << "Description: " << firstask.taskDescription << std::endl;

    std::cout << "Enter task name: ";
    std::getline(std::cin,name);
    std::cout << "Enter task description: ";
    std::getline(std::cin,description);

    firstask.changeDescription(description);
    firstask.changeName(name);

    std::cout << "Current task name and description" << std:: endl
    << "Name: " << firstask.taskName << std::endl
    << "Description: " << firstask.taskDescription << std::endl;
    */
    myTask.~task();
    return 0;
}