#include "tasks.h"
#include <iostream>
#include <fstream>
#include <vector>

int globalIndex;
const char* filePath = "/home/mintytrav/taskprogram/taskdata.dat";
const char* filePathTemp = "/home/mintytrav/taskprogram/tempfile.dat";

task::task()
 {
    taskName = "None";
    taskDescription = "None";
 }
task::task(std::string name)
 {
    taskName = name;
    taskDescription = "None";
 }
task::task (std::string name, std::string description)
 {
    taskName = name;
    taskDescription = description;
 }
 task::~task(){}
void task::changeName(std::string name)
 {
    taskName = name;
 }

void task::changeDescription(std::string description)
 {
    taskDescription = description;
 }

bool openfile(){
    std::string stringLine;
    std::ifstream infile;
    infile.open (filePath);
    if (infile.is_open()){
        infile >> globalIndex;
        infile.close();
    }
    else {
        return false;
    }
}

void task::addNewTask(){
    std::string name,description;
    std::cout <<"Enter task name: ";
    getline(std::cin,name);
    std::cout <<"Enter task description: ";
    getline(std::cin,description);
    
    taskName = name;
    taskDescription = description;

    std::fstream outfile;
    outfile.open(filePath);
    if (outfile.is_open()){
        outfile.seekp(0,outfile.end);
        outfile << "Task-" << std::endl;
        outfile << globalIndex +1 << std::endl;
        outfile << name << std::endl;
        outfile << description << std::endl;
        globalIndex++;
        
        outfile.seekp(0,outfile.beg);
        outfile << globalIndex;
        outfile.close();
        
        std::cout << "Task added succesfully!" << std::endl;
        std::cout << "Number of tasks: " << globalIndex << std::endl;
    }
    else{
        std::cout << "Error: Unable to open file to add task!" << std::endl;
    }

}

//to - do some bugs when index is out of range, need to fix. Make sure global index stays correct.
//
void task::removeTask(){
    int removeIndex = 0;
    int removeCompareIndex = 0;
    int moreIndex = 0;
    std::vector<std::string> removeVec;
    std::string fileLine;

    do{
        std::cout << "You have '" << globalIndex <<"' tasks." << std::endl;    
        std::cout << "Enter the task index to remove (enter 0 to exit without removing): ";
        std::cin >> removeIndex;
        std::cout << "Global: " << globalIndex << " - > " << removeIndex << std::endl;
    
    }while(removeIndex > globalIndex && removeIndex <=0);
    if (removeIndex == 0){
        std::cout << "No tasks have been removed, exiting..." << std::endl;
        return;
    }
    
    std::ifstream outfile;
    std::ofstream tempfile;
    outfile.open(filePath);
    tempfile.open(filePathTemp);
    if (outfile.is_open()){
        std::getline(outfile,fileLine);
        tempfile << globalIndex-1 <<std::endl;
        while(!outfile.eof())
        {
            if(fileLine == "Task-")
            {
                tempfile << "Task-" << std::endl << removeCompareIndex+1 << std::endl;
                removeCompareIndex++;
                std::getline(outfile,fileLine);
                moreIndex = fileLine[0] - '0';
            }
            else if (fileLine == std::to_string(removeIndex)){

                    std::getline(outfile,fileLine);
                    std::getline(outfile,fileLine);
                    std::getline(outfile,fileLine);
                    std::getline(outfile,fileLine);
                    std::getline(outfile,fileLine);
                
                std::cout << "Removed task successfully" << std::endl;
                globalIndex--;
                moreIndex++;
            }
            else if (moreIndex != removeCompareIndex)
            {
                tempfile << fileLine << std::endl;
                std::getline(outfile,fileLine);
            }
            else
            {
                std::getline(outfile,fileLine);
                moreIndex++;
            }
            
            
        }
    }
    outfile.close();
    tempfile.close();
    remove(filePath);
    rename(filePathTemp,filePath);
}

void task::listTasks() {
    std::ifstream readFile;
    readFile.open(filePath);
    int control = 0;

    std::string relayLine;

    while (!readFile.eof())
    {
        getline(readFile, relayLine);
        if (control == 0){
            std::cout << "Total Number of tasks: " << relayLine << std::endl;
            control++;
        }
        else if (relayLine == "Task-")
        {
            getline(readFile, relayLine);
            std::cout << "----Task Number: " << relayLine << "----" << std::endl;
            getline(readFile,relayLine);
            std::cout << "Task Name: " << relayLine << std::endl;
            getline(readFile,relayLine);
            std::cout << "Description: " << relayLine << std::endl;
        }
        else
        {
            std::cout << "End of Tasks!" <<std::endl;
            
        }
        
    }
    readFile.close();
    return;

}

void task::clearAll(){
    globalIndex = 0;

    std::ofstream outfile;
    remove(filePath);
    
    outfile.open(filePath);

    outfile << globalIndex << std::endl;
    outfile.close();
    std::cout << "Task list has been successfully cleared" << std::endl;

}